#pragma once

// Boxes
//---------------------------------------------------------------------------------------------------
#define OVP_ClassId_BoxAlgorithm_MatlabScripting			OpenViBE::CIdentifier(0x03303E0E, 0x39FE10DF)
#define OVP_ClassId_BoxAlgorithm_MatlabScriptingDesc		OpenViBE::CIdentifier(0x46130E2F, 0x34F90BA1)

// Global defines
//---------------------------------------------------------------------------------------------------
#ifdef TARGET_HAS_ThirdPartyOpenViBEPluginsGlobalDefines
#include "ovp_global_defines.h"
#endif // TARGET_HAS_ThirdPartyOpenViBEPluginsGlobalDefines

#define OV_AttributeId_Box_FlagIsUnstable					OpenViBE::CIdentifier(0x666FFFFF, 0x666FFFFF)
