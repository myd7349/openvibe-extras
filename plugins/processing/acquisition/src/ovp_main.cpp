#include "ovp_defines.h"

#include "box-algorithms/ovpCBoxAlgorithmAcquisitionClient.h"

namespace OpenViBE {
namespace Plugins {
namespace Acquisition {

OVP_Declare_Begin()
	OVP_Declare_New(CBoxAlgorithmAcquisitionClientDesc)
OVP_Declare_End()

}  // namespace Acquisition
}  // namespace Plugins
}  // namespace OpenViBE
