///-------------------------------------------------------------------------------------------------
/// 
/// \file defines.hpp
/// \brief Defines list for Setting, Shortcut Macro and const.
/// \author Thibaut Monseigne (Inria).
/// \version 1.0.
/// \date 12/03/2020.
/// \copyright Copyright (C) 2022 Inria
///
/// This program is free software: you can redistribute it and/or modify
/// it under the terms of the GNU Affero General Public License as published
/// by the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU Affero General Public License for more details.
///
/// You should have received a copy of the GNU Affero General Public License
/// along with this program.  If not, see <https://www.gnu.org/licenses/>.
/// 
///-------------------------------------------------------------------------------------------------

#pragma once

// Boxes
//---------------------------------------------------------------------------------------------------
#define Box_Hello_World					OpenViBE::CIdentifier(0x46705bc3, 0x3058a939)
#define Box_Hello_World_Desc			OpenViBE::CIdentifier(0x3f921b93, 0xadb2b2ec)
#define Box_Hello_Sender				OpenViBE::CIdentifier(0x4cbf4237, 0x24f54a7c)
#define Box_Hello_Sender_Desc			OpenViBE::CIdentifier(0x5730a8f1, 0x96a608cc)
#define Box_Hello_Bidirectionnal		OpenViBE::CIdentifier(0x47616d65, 0x42696469)
#define Box_Hello_Bidirectionnal_Desc	OpenViBE::CIdentifier(0x47616d65, 0x42696464)
