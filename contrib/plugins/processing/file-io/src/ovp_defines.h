#pragma once

// Boxes
//---------------------------------------------------------------------------------------------------
#define OVP_ClassId_BoxAlgorithm_EDFFileWriter    				OpenViBE::CIdentifier(0x0D454DCE, 0x470A4C02)
#define OVP_ClassId_BoxAlgorithm_EDFFileWriterDesc				OpenViBE::CIdentifier(0x0D454DCE, 0x470A4C02)
#define OVP_ClassId_BoxAlgorithm_BrainampFileWriterGipsa    	OpenViBE::CIdentifier(0x0C7E0BDE, 0x4EC90F95)
#define OVP_ClassId_BoxAlgorithm_BrainampFileWriterGipsaDesc	OpenViBE::CIdentifier(0x0A77142C, 0x316B6E47)

#define OVP_TypeId_BinaryFormat            						OpenViBE::CIdentifier(0x567234C5, 0x3D870DC3)
#define OVP_TypeId_BinaryFormat_int16_t    						OpenViBE::CIdentifier(0x1C777556, 0x123861C3)
#define OVP_TypeId_BinaryFormat_uint16_t   						OpenViBE::CIdentifier(0x6A7B6C73, 0x4B9D129D)
#define OVP_TypeId_BinaryFormat_float      						OpenViBE::CIdentifier(0x15183866, 0x7AAC69FC)
