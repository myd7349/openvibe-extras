#pragma once

// Boxes
//---------------------------------------------------------------------------------------------------
#define OVP_ClassId_BoxAlgorithm_OSCController 						OpenViBE::CIdentifier(0xC66F2F0C, 0x3BA5B424)
#define OVP_ClassId_BoxAlgorithm_OSCControllerDesc 					OpenViBE::CIdentifier(0xF7A35BD7, 0x6331C7D9)
#define OVP_ClassId_BoxAlgorithm_CBoxAlgorithmLSLExportGipsa 	 	OpenViBE::CIdentifier(0x591D2E94, 0x221C23AD)
#define OVP_ClassId_BoxAlgorithm_CBoxAlgorithmLSLExportGipsaDesc 	OpenViBE::CIdentifier(0x22AF11F5, 0x58F2787D)

// Global defines
//---------------------------------------------------------------------------------------------------
#ifdef TARGET_HAS_ThirdPartyOpenViBEPluginsGlobalDefines
#include "ovp_global_defines.h"
#endif // TARGET_HAS_ThirdPartyOpenViBEPluginsGlobalDefines

#define OV_AttributeId_Box_FlagIsUnstable							OpenViBE::CIdentifier(0x666FFFFF, 0x666FFFFF)
