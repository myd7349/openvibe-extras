# ---------------------------------
# Finds GTK toolkit
#
# Sets GTK_FOUND
# Sets GTK_LIBRARIES
# Sets GTK_LIBRARY_DIRS
# Sets GTK_LDFLAGS
# Sets GTK_LDFLAGS_OTHERS
# Sets GTK_INCLUDE_DIRS
# Sets GTK_CFLAGS
# Sets GTK_CFLAGS_OTHERS
# ---------------------------------

get_property(OV_PRINTED GLOBAL PROPERTY OV_TRIED_ThirdPartyGTK)

if(OV_DISABLE_GTK)
  OV_PRINT(OV_PRINTED  "  GTK disabled")
  return()
endif(OV_DISABLE_GTK)

include("FindThirdPartyPkgConfig")
if(WIN32)
	pkg_check_modules(GTK gtk+-win32-2.0 gthread-2.0)
else(WIN32)
	pkg_check_modules(GTK "gtk+-2.0" "gthread-2.0")
endif(WIN32)

if(${BUILD_ARCH} STREQUAL "x64")
	set(GTK_LIB_SUBFOLDER "i686-pc-vs10")
	set(LIB_Z_NAME "zlib1")
else()
	set(GTK_LIB_SUBFOLDER "2.10.0")
	set(LIB_Z_NAME "zdll")
endif()
if(GTK_FOUND)
	OV_PRINT(OV_PRINTED "  Found GTK+...")

	# optimistic...
	set(GTK_FOUND_EVERYTHING "True")
	
	#shouldn't add GTK_CFLAGS, this results in AdditionalIncludeDirectories becoming broken in visual studio
	#ADD_DEFINITIONS(${GTK_CFLAGS} ${GTK_CFLAGS_OTHERS} ${GTHREAD_CFLAGS}${GTHREAD_CFLAGS_OTHERS})
	#LINK_DIRECTORIES(${GTK_LIBRARY_DIRS} ${GTHREAD_LIBRARY_DIRS})
	if(WIN32)
		set( GTK_LIB_LIST ${GTK_LIBRARIES} ${GTHREAD_LIBRARIES} ${LIB_Z_NAME})
	else(WIN32)
		set( GTK_LIB_LIST ${GTK_LIBRARIES} ${GTHREAD_LIBRARIES} z)
	endif(WIN32)

	if(WIN32)
		# gdi32.lib could be under the MS Windows SDK
		include("OvSetWindowsSDKPath")
	endif(WIN32)
	
	foreach(GTK_LIB ${GTK_LIB_LIST})
		set(GTK_LIB1 "GTK_LIB1-NOTFOUND")
		find_library(GTK_LIB1 NAMES ${GTK_LIB} PATHS ${GTK_LIBRARY_DIRS} ${GTK_LIBDIR} NO_DEFAULT_PATH)
		find_library(GTK_LIB1 NAMES ${GTK_LIB} PATHS ${GTK_LIBRARY_DIRS} ${GTK_LIBDIR})
		if(WIN32)
			find_library(GTK_LIB1 NAMES ${GTK_LIB} PATHS ${OV_MS_SDK_PATH}/lib)
		endif(WIN32)
		if(GTK_LIB1)
			OV_PRINT(OV_PRINTED "    [  OK  ] Third party lib ${GTK_LIB1}")
			target_link_libraries(${PROJECT_NAME} ${GTK_LIB1})
		else(GTK_LIB1)
			OV_PRINT(OV_PRINTED "    [FAILED] Third party lib ${GTK_LIB}")
			set(GTK_FOUND_EVERYTHING "-NOTFOUND")
		endif(GTK_LIB1)
	endforeach(GTK_LIB)
endif(GTK_FOUND)

if(GTK_FOUND_EVERYTHING)
	include_directories(${GTK_INCLUDE_DIRS} ${GTHREAD_INCLUDE_DIRS})
	add_definitions(-DTARGET_HAS_ThirdPartyGTK)
else(GTK_FOUND_EVERYTHING)
	OV_PRINT(OV_PRINTED "  FAILED to find GTK+ or its components...")
	set(OV_DISABLE_GTK TRUE)
	if(NOT PKG_CONFIG_FOUND) 
		OV_PRINT(OV_PRINTED "    Did not even find pkg-config exe")
	endif(NOT PKG_CONFIG_FOUND)
endif(GTK_FOUND_EVERYTHING)

set_property(GLOBAL PROPERTY OV_TRIED_ThirdPartyGTK "Yes")

